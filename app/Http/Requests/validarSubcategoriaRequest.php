<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validarSubcategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'idsubcategoria' => 'numeric',
            'nombre' => 'required|max:255',
            'idcategoria' => 'required|numeric',
            'observacion' => 'max:500'
       ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'idsubcategoria.numeric' => '- La subcategoria ingresada no es valida',
            'nombre.required' => '- El nombre de la categoria es requerido',
            'nombre.max:255' => '- El nombre de la categoria no puede tener mas de 255 caracteres',
            'idcategoria.required' => '- Debe ingresar una categoria',
            'idcategoria.numeric' => '- La categoria ingresada no es valida',
            'observacion.max:255' => '- La observacion no puede tener mas de 500 caracteres'
        ];
    }
}
