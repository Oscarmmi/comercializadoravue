<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validarCategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'idcategoria' => 'numeric',
            'nombre' => 'required|max:255',
            'observacion' => 'max:500'
       ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'idcategoria.numeric' => '- La categoria ingresada no es valida',
            'nombre.required' => '- El nombre de la categoria es requerido',
            'nombre.max:255' => '- El nombre de la categoria no puede tener mas de 255 caracteres',
            'observacion.max:255' => '- La observacion no puede tener mas de 500 caracteres'
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'nombre' => 'trim|strtolower|escape',
            'observacion' => 'trim|strtolower|escape'
        ];
    }
}
