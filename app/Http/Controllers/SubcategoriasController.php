<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\validarSubcategoriaRequest;

use App\Subcategorias;

class SubcategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $id=$request->input('id');
            $datos=array();
            if(!$id){
                $aWhere=array();
                $nombre=$request->input('nombre');
                if($nombre){
                    $aWhere[]=array('subcategorias.nombre', 'LIKE', '%'.strtolower($nombre).'%');
                }
                $idcategoria=$request->input('idcategoria');
                if($idcategoria){
                    $aWhere[]=array('subcategorias.idcategoria', '=', $idcategoria);
                }
                $datos=DB::table('subcategorias')
                ->join('categorias', 'categorias.idcategoria', '=', 'subcategorias.idcategoria')
                ->select('subcategorias.idsubcategoria as id', 'subcategorias.nombre', 'categorias.nombre as categoria', 'subcategorias.observacion')
                ->where($aWhere) 
                ->get();
            }else{
                $datos=DB::table('subcategorias')
                ->select('subcategorias.*')
                ->where('idsubcategoria', '=', $id)
                ->get();       
            }
            return $datos;            
        }else{
            return view('home');
        }
    }

    public function buscarCategorias(){
        return DB::table('categorias')
            ->select('categorias.idcategoria as value', 'categorias.nombre as text')
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(validarSubcategoriaRequest $request)
    {
        $validatedData = $request->validated();
        $subcategoria = new Subcategorias();        
        return $this->guardarRegistro($validatedData, $subcategoria); 
    }

    public function validarNombreSubcategoriaUnico($aDatos){
        $aWhere=array();
        $aWhere[]=array('nombre', '=', $aDatos['nombre']);
        $aWhere[]=array('idcategoria', '=', $aDatos['idcategoria']);
        if(isset($aDatos['idsubcategoria'])){
            $aWhere[]=array('idsubcategoria', '!=', $aDatos['idsubcategoria']);
        }
        $datos=DB::table('subcategorias')
                ->select('subcategorias.*')
                ->where($aWhere)
                ->get();
        if(count($datos)){
            throw ValidationException::withMessages(['nombre' => '- El nombre de la subcategoria ya se encuentra registrado']);
        }
    }

    public function guardarRegistro($aDatos, $subcategoria){
        $this->validarNombreSubcategoriaUnico($aDatos);
        $subcategoria->nombre = $aDatos['nombre'];
        $observacion="";
        if(isset($aDatos['observacion'])){
            $observacion=$aDatos['observacion'];
        }
        $idusuario= auth()->user()->id;
        $subcategoria->user_id=$idusuario;
        $subcategoria->observacion = $observacion;
        $subcategoria->idcategoria = $aDatos['idcategoria'];
        $subcategoria->save();
        return $subcategoria;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(validarSubcategoriaRequest $request, $id)
    {
        $validatedData = $request->validated();
        $subcategoria = Subcategorias::find($id);        
        return $this->guardarRegistro($validatedData, $subcategoria); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategoria = Subcategorias::find($id);
        $subcategoria->delete();
    }
}
