<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\validarSubserieRequest;
use App\Http\Requests\validarrelacionarModelosRequest;

use App\Subseries;
use App\RelSubseriesAnnos;

class SubseriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $id=$request->input('id');
            $datos=array();
            if(!$id){                
                $datos=$this->buscarSubseriesxFiltros(array(
                    'nombre'=>$request->input('nombre'), 
                    'idmarca'=>$request->input('idmarca'), 
                    'idserie'=>$request->input('idserie')
                ));                
            }else{
                $aSubserie=DB::table('subseries')
                ->join('series', 'series.idserie', '=', 'subseries.idserie')
                ->select('subseries.*', 'series.idmarca')
                ->where('subseries.idsubserie', '=', $id)
                ->get();       
                $relSubSeriesAnnos=$this->buscarAnnosRelacionadosSubserie($id);
                $datos=array(
                    'datos'=>$aSubserie, 
                    'annos'=>$relSubSeriesAnnos
                );
            }
            return $datos;            
        }else{
            return view('home');
        }
    }

    public function buscarSubseriesxFiltros($aFiltros){
        $aWhere=array();
        if(isset($aFiltros['nombre'])){
            $aWhere[]=array('subseries.nombre', 'LIKE', '%'.strtolower($aFiltros['nombre']).'%');
        }
        if(isset($aFiltros['idmarca'])){
            $aWhere[]=array('series.idmarca', '=', $aFiltros['idmarca']);
        }
        if(isset($aFiltros['idserie'])){
            $aWhere[]=array('series.idserie', '=', $aFiltros['idserie']);
        }
        return DB::table('subseries')
        ->join('series', 'series.idserie', '=', 'subseries.idserie')
        ->join('marcas', 'marcas.idmarca', '=', 'series.idmarca')
        ->select('subseries.idsubserie as id', 'subseries.nombre', 'series.nombre as serie', 'marcas.nombre as marca', 'subseries.observacion')
        ->where($aWhere) 
        ->get();
    }

    public function buscarAnnos(){
        return DB::table('annos')
            ->select('annos.idanno as value', 'annos.anno as text')
            ->get();
    }

    public function buscarSeries(Request $request){
        $idmarca=$request->input('idmarca');
        if(!isset($idmarca)){
            throw ValidationException::withMessages(['marca' => '- Debe seleccionar una marca']);
        }
        $aWhere=array();
        $aWhere[]=array('series.idmarca','=', $idmarca);
        return DB::table('series')
            ->select('series.idserie as value', 'series.nombre as text')
            ->where($aWhere)
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(validarSubserieRequest $request)
    {
        $validatedData = $request->validated();
        $subserie = new Subseries();        
        $subserie = $this->guardarRegistro($validatedData, $subserie);  
        return $subserie;
    }

    public function validarNombreSubserieUnico($aDatos){
        $aWhere=array();
        if(!isset($aDatos['nombre'])){
            throw ValidationException::withMessages(['nombre' => '- El nombre de la subserie no puede estar vacio']);
        }
        $aDatos['nombre']=strtolower(trim($aDatos['nombre']));
        $aWhere[]=array('nombre', '=', $aDatos['nombre']);
        $aWhere[]=array('idserie', '=', $aDatos['idserie']);
        if(isset($aDatos['idsubserie'])){
            $aWhere[]=array('idsubserie', '!=', $aDatos['idsubserie']);
        }
        $datos=DB::table('subseries')
                ->select('subseries.*')
                ->where($aWhere)
                ->get();
        if(count($datos)){
            throw ValidationException::withMessages(['nombre' => '- El nombre de la subserie ya se encuentra registrado']);
        }
    }

    public function relacionarModelos(validarrelacionarModelosRequest $request){
        $validatedData = $request->validated();
        $aWhere=array();
        $aWhere[]=array('idsubserie', '=', $validatedData['idsubserie']);
        $aSubSerie=DB::table('subseries')
                ->select('subseries.*')
                ->where($aWhere)
                ->get();
        if(!count($aSubSerie)){
            throw ValidationException::withMessages(['subserie' => '- La subserie ingresada no es valida']);
        }
        if(isset($validatedData['final']) && $validatedData['inicio']>$validatedData['final']){
            throw ValidationException::withMessages(['años' => '- El año inicial no puede ser mayor al año final']);
        }
        if(isset($validatedData['final'])){
            $aAnnos=DB::table('annos')
                ->select('*')
                ->whereBetween('idanno', [$validatedData['inicio'], $validatedData['final']])
                ->get();
        }else{
            $filtrosAnnos=array();
            $filtrosAnnos[]=array('idanno', '=', $validatedData['inicio']);
            $aAnnos=DB::table('annos')
                ->select('*')
                ->where($filtrosAnnos)
                ->get();
        }        
        if(!count($aAnnos)){
            throw ValidationException::withMessages(['años' => '- Uno o ambos de los años ingresados no es valido']);
        }
        $modelosRelacionados=DB::table('relsubseriesannos')
                ->select('relsubseriesannos.idanno')
                ->where($aWhere)
                ->get();
        foreach($aAnnos as $anno){
            $crear=1;
            foreach($modelosRelacionados as $modelo){
                if(intval($modelo->idanno)===intval($anno->idanno)){
                    $crear=0;
                    break;
                }
            }
            if(!$crear){
                continue;
            }
            $eRel=new RelSubseriesAnnos();          
            $eRel->idsubserie=$validatedData['idsubserie'];
            $eRel->idanno = $anno->idanno;
            $eRel->save();
        }
        return $this->buscarAnnosRelacionadosSubserie($validatedData['idsubserie']);
    }
        
    public function buscarAnnosRelacionadosSubserie($idsubserie){
        $aWhere=array();
        $aWhere[]=array('idsubserie', '=',$idsubserie);
        return DB::table('relsubseriesannos')
            ->join('annos', 'annos.idanno', '=', 'relsubseriesannos.idanno')
            ->select('relsubseriesannos.idrelsubserieanno', 'annos.anno as modelo')
            ->where($aWhere)
            ->get();
    }

    public function guardarRegistro($aDatos, $subserie){
        $this->validarNombreSubserieUnico($aDatos);
        $subserie->nombre = $aDatos['nombre'];
        $observacion="";
        if(isset($aDatos['observacion'])){
            $observacion=$aDatos['observacion'];
        }
        $idusuario= auth()->user()->id;
        $subserie->user_id=$idusuario;
        $subserie->observacion = $observacion;
        $subserie->idserie = $aDatos['idserie'];
        $subserie->save();
        return $subserie;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(validarSubserieRequest $request, $id)
    {
        $validatedData = $request->validated();
        $subserie = Subseries::find($id);        
        return $this->guardarRegistro($validatedData, $subserie);  
    }

    public function eliminarRelSubserieAnno(Request $request){
        $idrelsubserieanno=$request->input('idrelsubserieanno');
        $eRel = RelSubseriesAnnos::find($idrelsubserieanno);
        $idsubserie=$eRel['idsubserie'];
        $eRel->delete();  
        return $this->buscarAnnosRelacionadosSubserie($idsubserie);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subserie = Subseries::find($id);
        $subserie->delete();       
    }
}
