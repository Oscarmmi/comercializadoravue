<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelSubseriesAnnos extends Model
{
    public $table = "relsubseriesannos";
    protected $primaryKey = 'idrelsubserieanno';
}
