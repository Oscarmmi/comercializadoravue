import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
      estadosComponentes:{
          'marcas':1, 
          'series':1, 
          'subseries':1, 
          'categorias':1,
          'subcategorias':1
      }
    },
    getters: {
        buscarEstadoComponentes: state=>{
            return state.estadosComponentes;
        }
    },
    mutations: {
        desactivarActivarComponentes (state, aDatos) {
            setTimeout(function() {
                state.estadosComponentes[aDatos.componente]=aDatos.accion;
              }, 250);            
        }
    },
    actions: {

    }
});