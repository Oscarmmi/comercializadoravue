require('./bootstrap');

import Vue from 'vue';
import 'es6-promise/auto';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Toasted from 'vue-toasted';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import store from './store/store.js';

library.add(faCoffee);
Vue.component('font-awesome-icon', FontAwesomeIcon);
library.add(fas, fab);
Vue.config.productionTip = false;

window.Vue = require('vue');
Vue.use(BootstrapVue);
Vue.use(Toasted);
Vue.component('icon', FontAwesomeIcon);
Vue.component('inicio', require('./components/InicioComponent.vue').default);
Vue.component('sidebar', require('./components/SideBarComponent.vue').default);
Vue.component('marcas', require('./components/MarcasComponent.vue').default);
Vue.component('series', require('./components/SeriesComponent.vue').default);
Vue.component('subseries', require('./components/SubseriesComponent.vue').default);
Vue.component('subcategorias', require('./components/SubcategoriasComponent.vue').default);
Vue.component('categorias', require('./components/CategoriasComponent.vue').default);


const app = new Vue({
    el: '#app',  
    store, 
    data:{

    }
});
