@extends('layouts.app')

@section('content')   
    <b-container class="row col-md-12">
        <subseries></subseries>
        <series></series>
        <marcas></marcas>       
        <categorias></categorias> 
        <subcategorias></subcategorias>
    </b-container>
@endsection
