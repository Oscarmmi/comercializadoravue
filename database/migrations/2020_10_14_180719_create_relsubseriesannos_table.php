<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelSubseriesAnnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relsubseriesannos', function (Blueprint $table) {
            $table->bigIncrements('idrelsubserieanno');
            $table->bigInteger('idsubserie')->unsigned();
            $table->bigInteger('idanno')->unsigned();
            $table->text('observacion')->nullable();
            $table->foreign('idsubserie')->references('subseries')->on('idsubserie');
            $table->foreign('idanno')->references('idanno')->on('annos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relsubseriesannos');
    }
}
